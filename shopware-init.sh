#!/bin/bash

apt-get update && apt-get -y install php-redis && phpenmod redis
time sync &
apt-get clean;


test -e /apache-extra-config  || mkdir /apache-extra-config


#########
echo -n HTPASS:
test -f /var/htpass/.htpasswd && rm var/htpass/.htpasswd
[ ! -z ${TOKEN_USER} ] && [  ! -z ${TOKEN_PASS} ] && ( echo "HTPASS-GEN"
failed=no

if [ -z ${TOKEN_USER} ] ;then failed=yes;fi
if [ -z ${TOKEN_PASS} ] ;then failed=yes;fi

echo "$failed" |grep -q yes && ( echo "TOKEN_PASS OR TOKEN_USER MISSING , generating RANDOM HTPASSWD ";TOKEN_USER="$(cat /dev/urandom |tr -cd '[:alnum:]_\-'  |head -c 10)" ;TOKEN_PASS="$(cat /dev/urandom |tr -cd '[:alnum:]_\-,.'  |head -c 24)"  ; htpasswd -cbBC 10 /var/htpass/.htpasswd "${TOKEN_USER}" "${TOKEN_PASS}" )
echo "$failed" |grep -q yes || (echo "GENERATING HTPASSWD FROM ENV"; htpasswd -cbBC 10 /var/htpass/.htpasswd "${TOKEN_USER}" "${TOKEN_PASS}" )

)

user_htpass=no

if [ -z ${TOKEN_GUEST_PASS} ] ;then user_htpass=no;else user_htpass=yes;fi


echo $user_htpass |grep ^yes$ ||  ( cat /var/htpass/.htpasswd > /var/htpass/.htpasswd.user )

echo $user_htpass |grep ^yes$ && {

echo "HTPASS-GUEST-GEN ( username : users )"

test -f /var/htpass/.htpasswd || (echo "GENERATING HTPASSWD FROM ENV"; htpasswd -bBC 10 /var/htpass/.htpasswd.user "users" "${TOKEN_GUEST_PASS}" )

test -f /var/htpass/.htpasswd && ( cat /var/htpass/.htpasswd > /var/htpass/.htpasswd.user ;(echo "GENERATING HTPASSWD FROM ENV"; htpasswd -bBC 10 /var/htpass/.htpasswd.user "users" "${TOKEN_GUEST_PASS}" ) )


    }
# echo "ssh keys"
 touch /var/www/.ssh/authorized_keys && chown root:root /var/www/.ssh /var/www/.ssh/authorized_keys && chmod go-rw  /root/.ssh/authorized_keys /root/.ssh /var/www/.ssh /var/www/.ssh/authorized_keys


## now apache fixes
for myconf in /etc/apache2/sites-available.default/000-default.conf  /etc/apache2/sites-available.default/default-ssl.conf  /etc/apache2/sites-enabled/000-default.conf  /etc/apache2/sites-enabled/default-ssl.conf  ;do 

#merge file content
#ls -1 ${myconf}|grep -q ${myconf} && grep -q "shopware - Block access to miscellaneous protected files" "${myconf}" ||  sed '/\(.\+\)DocumentRoot\(.\+\)/ r /shopwarefixes.conf'  "${myconf}" -i

ls -1 ${myconf}|grep -q ${myconf} && grep -q "Include /shopwarefixes.conf" "${myconf}" ||  sed 's/\(.\+\)DocumentRoot\(.\+\)/\0\n        Include \/shopwarefixes.conf\n/g'  "${myconf}" -i


done



which redis-server && ( echo "setting up redis sessionstorage"; 
    for phpconf in $(find $(find /etc/ -maxdepth 1 -name "php*") -name php.ini |grep -e apache -e fpm);do 
       grep "session.save_handler = redis" "${phpconf}"                || ( echo "session.save_handler = redis"   |tee -a "${phpconf}" )
       grep 'session.save_path = "tcp://127.0.0.1:6379"'  "${phpconf}" || ( echo 'session.save_path = "tcp://127.0.0.1:6379"' |tee -a "${phpconf}" )
    done
    )

which redis-server || ( echo "no redis found;disabling redis session storage"; 
    for phpconf in $(find $(find /etc/ -maxdepth 1 -name "php*") -name php.ini |grep -e apache -e fpm);do 
        sed 's/session.save_path.\+tcp.\+:6379.\+//g' "${phpconf}"  -i
        sed 's/session.save_handler = redis//g' "${phpconf}" -i
    done
    )


ln -s /usr/bin/msmtp /usr/sbin/sendmail.real
wget -c https://gitlab.com/the-foundation/msmtp-cron-sendmail/-/raw/master/sendmail -O /tmp/sendmail
sed 's/\r//' /tmp/sendmail -i
grep -q -e 'test -e /etc/sendmail.msmtp.conf' /tmp/sendmail && grep -q -e 'exit "$Status"' /tmp/sendmail && chmod +x /tmp/sendmail ;
mv /tmp/sendmail /usr/sbin/sendmail

